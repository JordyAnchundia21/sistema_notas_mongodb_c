const {Router} = require ('express');
const router = Router();

const {
    renderNoteForm, 
    createNewNote, 
    renderNotes, 
    renderEditForm, 
    updateNote, 
    deletenote
} = require ('../controllers/notes.controller');

const {isAuthenticated} = require('../helpers/auth')
// nuevas notas
router.get('/notes/add', isAuthenticated, renderNoteForm);

router.post('/notes/new-note', isAuthenticated, createNewNote);

// Get All Note
router.get('/notes', isAuthenticated, renderNotes);

// Editar Notas
router.get('/notes/edit/:id', isAuthenticated, renderEditForm)

router.put('/notes/edit/:id', isAuthenticated, updateNote)

// Eliminar Notas
router.delete('/notes/delete/:id', isAuthenticated, deletenote)

module.exports = router;